module.exports = {
  apps: [
    {
      name: "Cybermiles.Control",
      script: "index.js",
      env: {
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ]
};
