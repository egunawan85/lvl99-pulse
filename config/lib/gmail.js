const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");
const { google } = require("googleapis");
const keys = require("../keys");

const options = {
	auth: {
		api_key: keys.sendGridKey
	}
};

module.exports = nodemailer.createTransport(sgTransport(options));
