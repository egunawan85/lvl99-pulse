const keys = require("../keys");
const twilio = require("twilio")(keys.twilioAccountSID, keys.twilioAuthToken);

module.exports = twilio;
