const CronJob = require("cron").CronJob;
const axios = require("axios");
const gmail = require("./config/lib/gmail");
const twilio = require("./config/lib/twilio");
const contacts = require("./config/contacts");
const keys = require("./config/keys");

new CronJob(
	"* * * * *",
	async () => {
		const result = await axios.get("http://206.189.144.103:26657/status");
		const catchingUp = result.data.result.sync_info.catching_up;
		if (result.status != 200) {
			let mail = {
				from: contacts.EMAIL_FROM,
				to: contacts.EMAIL_TO,
				subject: "Urgent: Cybermiles Validator Offline",
				generateTextFromHTML: true,
				html: "Cybermiles Validator is not responding"
			};
			gmail.sendMail(mail, (error, response) => {
				error ? console.log(error) : console.log(response);
				gmail.close();
			});

			twilio.messages.create({
				from: contacts.SMS_FROM,
				to: contacts.SMS_TO,
				body: "Urgent: Cybermiles Validator is not responding"
			});
		}
	},
	null,
	true,
	"Asia/Jakarta"
);

new CronJob(
	"*/20 * * * *",
	async () => {
		const pubKey = keys.cybermilesPubKey;
		const status = await axios.get("http://206.189.144.103:26657/status");
		const height = status.data.result.sync_info.latest_block_height;
		const nBlocks = 100;

		let n = 0;
		let tally = 0;
		let found = false;
		while (nBlocks > n) {
			let block = height - n;
			const results = await axios.get("http://206.189.144.103:26657/block?height=" + block);
			const validatorSigns = results.data.result.block.last_commit.precommits;
			found = validatorSigns.some(sign => {
				return sign.validator_address === pubKey;
			});
			if (found) {
				tally++;
				console.log("block: " + validatorSigns[0].height + "tally: " + tally);
				found = false;
			}
			n++;
		}

		if (tally != nBlocks) {
			let mail = {
				from: contacts.EMAIL_FROM,
				to: contacts.EMAIL_TO,
				subject: "Missed Blocks Report",
				generateTextFromHTML: true,
				html: "Cybermiles Node Missed some blocks. Tally: " + tally + " Blocks: " + nBlocks
			};
			gmail.sendMail(mail, (error, response) => {
				error ? console.log(error) : console.log(response);
				gmail.close();
			});

			twilio.messages.create({
				from: contacts.SMS_FROM,
				to: contacts.SMS_TO,
				body: "Cybermiles Node Missed some blocks. Tally: " + tally + " Blocks: " + nBlocks
			});
		}
	},
	null,
	true,
	"Asia/Jakarta"
);
